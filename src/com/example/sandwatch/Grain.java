package com.example.sandwatch;

import java.util.Random;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
 public class Grain {
       
    private static final int BMP_ROWS = 2;
    private static final int BMP_COLUMNS = 2;
    private WatchView watchView;
    
    private int x = 5;
    private int y = 10;       
    private int xSpeed = 0;    
    private int ySpeed = 1;       
    private int currentFrame = 0;   
    private int width;       
    private int height;
    private int r=10;
    private Paint paint = new Paint();
   
     public Grain(WatchView watchView, float x, float y) 
     {
           this.watchView = watchView;
           
           //this.width = bmp.getWidth() / BMP_COLUMNS;
           //this.height = bmp.getHeight() / BMP_ROWS;
           this.x=(int) x;
           this.y=(int) y;
           //Random rnd = new Random();
           //xSpeed = rnd.nextInt(10)-5;
           ySpeed = 5;
     }

     
     private void update() 
     {
         if (x >= watchView.getWidth()-r) 
         {
             xSpeed = -xSpeed;
         }
         
         x = x + xSpeed;
         
         if (y >= watchView.getHeight()) 
         {		
             y=0;
         }
         
         y = y + ySpeed;
         currentFrame = ++currentFrame % BMP_COLUMNS;
     }

    /* public void setPosition(float x, float y) {
    	 this.x=(int) x;
    	 this.y=(int) y;
     }*/
     public void onDraw(Canvas canvas) 
     {	
         update();
         int srcX = currentFrame * width;
         int srcY = 1 * height;
         //Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
         //Rect dst = new Rect(x, y, x + width, y + height);
         paint.setColor(Color.BLACK);
         canvas.drawCircle(x, y, r, paint);
         
     }
 }