package com.example.sandwatch;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
@SuppressLint("WrongCall")
public class WatchView extends SurfaceView {
	
       private Bitmap bmp;
       private SurfaceHolder holder;
       private manager loopThread;
       private Grain grain;
       private int num=0;
       public int x,y;
       private ArrayList<Grain> grains = new ArrayList<Grain>(30);
       private int in=0; 
       
       public WatchView(Context context) 
       {
    	   super(context);
    	   loopThread= new manager(this);
           holder = getHolder();
           holder.addCallback(new SurfaceHolder.Callback() 
           {
                  public void surfaceDestroyed(SurfaceHolder holder) 
                  {
                	  boolean retry = true;
                      loopThread.setRunning(false);
                      while (retry) {
                             try {
                                   loopThread.join();
                                   retry = false;
                             } catch (InterruptedException e) {
                             }
                      }
                  }

                  @Override
                  public void surfaceCreated(SurfaceHolder holder) 
                  {
                	  loopThread.setRunning(true);
                      loopThread.start();
                  }

                  @Override
                  public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) 
                  {
                  }
           });
           //bmp = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
           
           
     }

      
       Paint paint = new Paint();
    
	protected void onDraw(Canvas canvas) 
     {
    	 paint.setColor(Color.BLACK);
    	 canvas.drawColor(Color.WHITE);
    	 
    	 //grain.onDraw(canvas);
    	 	 if (!grains.isEmpty()) {
    	 		synchronized (this) { for(Grain grain : grains) {
    			 grain.onDraw(canvas);
    			}}
    	 	 }
    	 }
    	 
    	 
 	 
     
	
	     
     public boolean onTouchEvent(MotionEvent event) {
    	 synchronized (this) {grains.add(grain = new Grain(this,event.getX(),event.getY()));}
    	in++;
    	//x=(int) event.getX();
    	//y=(int) event.getY();
    	
        return super.onTouchEvent(event);
    }
     
     
}