package com.example.sandwatch;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
@SuppressLint("WrongCall")
public class manager extends Thread {
      
      private WatchView view;
     
      
      private boolean running = false;
     
      
      public manager(WatchView view) 
      {
            this.view = view;
      }

       
      public void setRunning(boolean run) 
      {
            running = run;
      }

     
      public void run() {
            while (running) {
                   Canvas c = null;
                   try {
                          c = view.getHolder().lockCanvas();
                          synchronized (view.getHolder()) {
                                 view.onDraw(c);
                          }
                   } finally {
                          if (c != null) {
                                 view.getHolder().unlockCanvasAndPost(c);
                          }
                   }
            }
      }
}  